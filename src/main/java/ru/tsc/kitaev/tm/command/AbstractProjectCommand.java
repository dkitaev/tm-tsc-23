package ru.tsc.kitaev.tm.command;

import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand{

    protected void showProject(@Nullable Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Id: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("Status: " + project.getStatus());
    }

}
