package ru.tsc.kitaev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.command.AbstractCommand;

public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "about";
    }

    @NotNull
    @Override
    public String arg() {
        return "-a";
    }

    @NotNull
    @Override
    public String description() {
        return "Display developer info...";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Danil Kitaev");
        System.out.println("E-MAIL: dkitaev@tsconsulting.com");
    }

}
