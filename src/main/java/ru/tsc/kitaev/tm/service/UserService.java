package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IUserService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.user.EmailExistsException;
import ru.tsc.kitaev.tm.exception.user.LoginExistsException;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public boolean isEmailExists(final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    public User removeUser(@Nullable final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        @NotNull final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return userRepository.add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (isEmailExists(email)) throw new EmailExistsException();
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (isLoginExists(login)) throw new LoginExistsException();
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public void setPassword(@Nullable final String userId, @Nullable final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = findById(userId);
        if (user == null) return;
        @NotNull final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
    }

    @Override
    public void updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @Nullable final User user = findById(userId);
        if (user == null) return;
        user.setFirstName(firstName);
        user.setLogin(lastName);
        user.setMiddleName(middleName);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
    }

}
