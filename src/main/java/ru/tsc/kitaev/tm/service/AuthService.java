package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.service.IAuthService;
import ru.tsc.kitaev.tm.api.service.IUserService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.empty.EmptyLoginException;
import ru.tsc.kitaev.tm.exception.empty.EmptyPasswordException;
import ru.tsc.kitaev.tm.exception.user.AccessDeniedException;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.util.HashUtil;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @Nullable
    private String userId;

    public AuthService(@NotNull final IUserService userService) {
        this.userService = userService;
    }

    @Nullable
    @Override
    public User getUser() {
        @NotNull final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        userService.create(login, password, email);
    }

    @Override
    public void checkRoles(final Role @Nullable... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final User user = getUser();
        if (user == null) throw new AccessDeniedException();
        @Nullable final Role role = user.getRole();
        for (@NotNull final Role item : roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

}
