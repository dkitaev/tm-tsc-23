package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IServiceLocator {

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

}
