package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IRepository;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class AbstractRepository <E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    @NotNull
    public static Predicate<AbstractEntity> predicateById(@NotNull final String id) {
        return s -> id.equals(s.getId());
    }

    @NotNull
    @Override
    public E add(@NotNull final E entity) {
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(@NotNull final E entity) {
        list.remove(entity);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @NotNull
    @Override
    public List<E> findAll() {
        return list;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull Comparator<E> comparator) {
        return list.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findById(@NotNull final String id) {
        return list.stream()
                .filter(predicateById(id))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final Integer index) {
        return list.get(index);
    }

    @Nullable
    @Override
    public E removeById(@NotNull final String id) {
        @Nullable final Optional<E> entity = Optional.ofNullable(findById(id));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(EntityNotFoundException::new);
    }

    @NotNull
    @Override
    public E removeByIndex(@NotNull final Integer index) {
        @NotNull final Optional<E> entity = Optional.of(findByIndex(index));
        entity.ifPresent(this::remove);
        return entity.orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        @Nullable final E entity = findById(id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final int index) {
        if (index < 0) return false;
        return index < list.size();
    }

    @NotNull
    @Override
    public Integer getSize() {
        return list.size();
    }

}
